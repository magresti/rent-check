import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from '../users.service';
import { User } from '../entities/user.entity';
import { UserCreationDTO } from '../entities/userCreation.dto';
import { AuthGuard } from 'src/common/guards/auth/auth/auth.guard';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  /**
   * Returns all the users
   * @returns Promise<User[]>
   */
  @UseGuards(AuthGuard)
  @Get()
  async getUsers(): Promise<User[]> {
    try {
      const users = await await this.usersService.findAll();
      console.log(users);
      return users;
    } catch (error) {
      console.error(error);
      throw new HttpException(
        'error fetching users',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  /**
   * Adds a new user and returns it
   * @param user
   * @returns Promise<User>
   */
  @UseGuards(AuthGuard)
  @Post()
  async addUser(@Body() user: UserCreationDTO): Promise<User> {
    try {
      const addUser = await this.usersService.addNew(user);
      return addUser;
    } catch (error) {
      console.error(error);
      throw new HttpException(
        'error creating new user',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
