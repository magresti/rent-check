import { IsNotEmpty, IsString } from 'class-validator';

export class UserCreationDTO {
  @IsString()
  @IsNotEmpty()
  public name: string;
  @IsString()
  @IsNotEmpty()
  public surname: string;
  @IsString()
  @IsNotEmpty()
  public username: string;
  @IsString()
  @IsNotEmpty()
  public email: string;
  @IsString()
  @IsNotEmpty()
  public password: string;
}
