import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/common/database/database.module';
import { UsersService } from './users.service';
import { usersProviders } from './users.provider';
import { UsersController } from './users/users.controller';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [DatabaseModule],
  providers: [...usersProviders, UsersService, ConfigService],
  controllers: [UsersController],
  exports: [UsersService],
})
export class UsersModule {}
