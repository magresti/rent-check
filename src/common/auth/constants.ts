export const jwtConstants = {
    JWT_SECRET: 'A_SUPER_SECRET_STRING',
    JWT_EXPIRATION_TIME:'60s'
}