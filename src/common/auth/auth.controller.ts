import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignInDTO } from './entities/signIn.dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  async signin(@Body() signinDTO: SignInDTO) {
    return await this.authService.signIn(
      signinDTO.username,
      signinDTO.password,
    );
  }
}
